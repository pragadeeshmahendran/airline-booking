import React, { Component } from 'react';
import './App.css';
import { Route, Switch, Redirect } from 'react-router';
import Header from './components/Header/Header';
import CheckIn from './containers/CheckIn/CheckIn';
import FlightDetail from './containers/FlightList/FlightDetail/FlightDetail';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route path="/checkin" exact component={CheckIn} />
          <Route path="/checkin/flight" component={FlightDetail} />
          <Route path="/" exact>
            Home
          </Route>
          <Redirect to="/" />
        </Switch>
      </div>
    );
  }
}

export default App;
