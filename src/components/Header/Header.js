import React from 'react';
import { Navbar } from 'react-bootstrap';
import BrandLogo from '../../assets/images/Logo.svg';

const Header = (props) => {
  return (
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand href="#home">
        <img
          alt=""
          src={BrandLogo}
          width="30"
          height="30"
          className="d-inline-block align-top"
        />{' '}
        Bragz-Jet
      </Navbar.Brand>
    </Navbar>
  );
};

export default Header;
