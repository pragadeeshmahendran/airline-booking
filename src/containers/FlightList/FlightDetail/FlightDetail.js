import React, { Component } from 'react';
import AddPassenger from '../../../components/AddPassenger/AddPassenger';

class FlightDetail extends Component {
    state = {
        flightDetail: {
            id: 1,
            name: 'AirDeccan',
            seats: [
                {id: 1},
                {id: 2},
                {id: 3},
                {id: 4},
                {id: 5}
            ],
            passengers: [
                {id: 1, seatNo: 1, gender: 'male', wheelchair: 0, infants: 1},
                {id: 2, seatNo: 2, gender: 'female', wheelchair: 1, infants: 0}
            ]
        },
        addPassenger: false
    }

    openCheckInModal = () => {
        setTimeout(() => {
            this.setState({...this.state, addPassenger: true});
        }, 10);  
    }

    render() {
        let flightDetail = this.state.flightDetail;
        return (
            
            <div className="FlightDetail">
                <h1>{this.state.name}</h1>
                <ul>
                {
                    flightDetail.seats.map(seat => {
                        let occupied = 0;
                        let infants = 0;
                        let wheelchair = 0;
                        let gender = 'Empty';
                        flightDetail.passengers.forEach(passenger => {
                            if(passenger.seatNo === seat.id) {
                                occupied = 1;
                                if(passenger.infants) {
                                    infants = 1;
                                }
                                if(passenger.wheelchair) {
                                    wheelchair = 1;
                                }
                                if(passenger.gender) {
                                    gender = passenger.gender;
                                }
                            }
                            
                        }); 
                        return <li key={seat.id}>
                            Seat No: {seat.id}

                            occupied: {occupied}
                            infants: {infants}
                            wheelchair: {wheelchair}
                            gender: {gender}
                            {occupied ? <button>-</button> : <button onClick={() => this.openCheckInModal() }>+</button> }
                        </li>
                    })
                }
                </ul>
                {
                    (this.state.addPassenger ? <AddPassenger /> : '')
                }
            </div>
        )
    }
}

export default FlightDetail;
