import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Container from '@material-ui/core/Container';

class FlightList extends Component {

  state = {
    flights: [
      {id: 1, name: 'AirDeccan', total: 5, available: 3},
      {id: 2, name: 'AirIndia', total: 5, available: 5},
    ]
  };

  useStyles = () => makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
  }));
  

  getStyles = () => this.useStyles();

  render() {
    return (
      
      <div className={this.getStyles.root}>
        <Container maxWidth="md">
        {this.state.flights.map((flight) => {
          return <Accordion key={flight.id}><AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={this.getStyles.heading}>{flight.name}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Total Seats: {flight.total}
            Available Seats: {flight.available}
            <br/>
            <a href={"/checkin/flight/" + flight.id}>Get More Details</a>
          </Typography>
        </AccordionDetails></Accordion>
        })}
        </Container>
      </div>
      
    );
  }
  
}

export default FlightList;